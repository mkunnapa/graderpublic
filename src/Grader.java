import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.apache.maven.shared.invoker.*;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.eclipse.jgit.util.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Mihkel Künnapas on 05.05.2016.
 */
public class Grader {
    static Logger log = LoggerFactory.getLogger(Grader.class);

    final static String BASE_DIRECTORY = "C:\\Homework";
    final static String USERS_FILE = BASE_DIRECTORY + "\\users.txt";

    public static void main(String[] args) {
        FileAppender fa = new FileAppender();
        fa.setName("FileLogger");
        fa.setFile("grader.log");
        fa.setLayout(new PatternLayout("%d %-5p [%c{1}] %m%n"));
        fa.setThreshold(Level.DEBUG);
        fa.setAppend(true);
        fa.activateOptions();

        BasicConfigurator.configure(fa);

        Grader grader = new Grader();
        List<String> userNames = null;

        try {
            userNames = grader.readUserNames();
            log.debug("Finished reading usernames");
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (String user : userNames) {
            try {
                grader.grade(user);
            } catch (GitAPIException e) {
                e.printStackTrace();
                continue;
            } catch (MavenInvocationException e) {
                e.printStackTrace();
                continue;
            }
        }
        try {
            grader.checkForPlagiarism();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String> readUserNames() throws IOException {
        log.debug("Reading usernames");
        Path path = Paths.get(USERS_FILE);
        return Files.readAllLines(path);
    }

    public void grade(String user) throws GitAPIException, MavenInvocationException {
        clone(user);

        Thread t1 = new Thread(new Runnable() {
            public void run() {
                try {
                    deploy(user);
                } catch (MavenInvocationException e) {
                    e.printStackTrace();
                }
            }
        });
        t1.setDaemon(true);
        t1.start();

        while (!deployComplete()) {
            try {
                log.debug("Deploy not complete, waiting for 3 seconds");
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        getGrade(user);
        t1.stop();

    }

    private void clone(String user) throws GitAPIException {
        String remoteURI = "https://bitbucket.org/" + user + "/i377";
        File userDirectory = new File(BASE_DIRECTORY + "\\" + user);

        log.debug(userDirectory.toString() + " exists, deleting");
        if (userDirectory.exists()) {
            try {
                FileUtils.delete(userDirectory, FileUtils.RECURSIVE);
                log.debug("deleted");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        log.debug("cloning repo for user " + user);

        String username = "*";
        String password = "*";

        Git.cloneRepository()
                .setURI(remoteURI)
                .setDirectory(userDirectory)
                .setCredentialsProvider(new UsernamePasswordCredentialsProvider(username, password))
                .call();
        log.debug("finished cloning repo for user " + user);
    }

    private void deploy(String user) throws MavenInvocationException {
        log.debug("deploying for user " + user);
        InvocationRequest request = new DefaultInvocationRequest();
        request.setPomFile(new File(BASE_DIRECTORY + "\\" + user + "\\pom.xml"));
        request.setGoals(Arrays.asList("tomcat7:run"));

        Invoker invoker = new DefaultInvoker();
        InvocationResult result = invoker.execute(request);

        if (result.getExitCode() != 0) {
            throw new IllegalStateException("Build failed for user " + user);
        }
        log.debug("finished deployment for user " + user);
    }

    private void getGrade(String user) {
        log.debug("Getting grade for user " + user);
        BatchRunner batchRunner = new BatchRunner();
        int points = batchRunner.runAllTests(user, user);
        log.debug("Got grade for user " + user + " points: " + points);
    }

    public void checkForPlagiarism() throws IOException {
        log.debug("Running JPlag");
        Runtime.getRuntime().exec("java -jar C:\\Homework\\jplag-2.11.8.jar -l java17 -r C:\\Homework\\Result -s -m 85% C:\\Homework");
        log.debug("Finished running JPlag");
    }

    private boolean deployComplete() {
        URL u = null;
        try {
            u = new URL("http://localhost:8080/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection huc = null;
        try {
            huc = (HttpURLConnection) u.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            huc.setRequestMethod("GET");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }
        try {
            huc.connect();
        } catch (IOException e) {
            return false;
        }
        int responseCode = 0;
        try {
            responseCode = huc.getResponseCode();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return responseCode == 200;
    }


}
