package tests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static pages.StaticMethods.*;

import org.junit.Before;
import org.junit.Test;

import pages.*;

public class Homework3bTest {

    @Before
    public void setUp() {
        Pages.setBaseUrl("http://mkalmo-hw3.appspot.com/");
    }

    @Test
    public void searchPageExists_10() {
        assertTrue(goToSearchPage().isSearchPage());
    }

    @Test
    public void addPageExists_5() {
        assertTrue(goToAddPage().isAddPage());
    }

    @Test
    public void searchPageHasValidMenu_2() {
        assertValid(goToSearchPage().menu());
    }

    @Test
    public void addPageHasValidMenu_2() {
        assertValid(goToAddPage().menu());
    }

    @Test
    public void customersCanBeAdded_2() {
        String testCode = randomString();

        SearchPage searchPage = goToAddPage().insert(testCode);

        assertThat(searchPage.getCodes(), hasItem(testCode));
    }

    @Test
    public void allDataCanBeDeleted_2() {
        SearchPage searchPage = goToSearchPage().menu().clearData();
        assertThat(searchPage.getCodes(), is(empty()));
    }

    @Test
    public void sampleDataCanBeInserted_3() {
        SearchPage searchPage = goToSearchPage().menu().clearData();
        searchPage.menu().insertSampleData();

        assertThat(searchPage.getCodes(), contains("123", "456", "789"));
    }

    @Test
    public void customersCanBeDeleted_2() {
        SearchPage searchPage = goToSearchPage().menu().clearData();
        String code1 = randomString();
        String code2 = randomString();

        searchPage = goToAddPage().insert(code1);
        searchPage = goToAddPage().insert(code2);
        searchPage.delete(code1);

        assertThat(searchPage.getCodes(), not(hasItem(code1)));
        assertThat(searchPage.getCodes(), hasItem(code2));
    }

    @Test
    public void listCanBeFiltered_2() {
        goToSearchPage().menu().clearData();
        goToSearchPage().menu().insertSampleData();

        SearchPage searchPage = goToSearchPage();
        searchPage = searchPage.search("Doe");

        assertThat(searchPage.getCodes(), contains("123", "456"));
    }

    @Test
    public void redirectsAfterInsert_1() {
        SearchPage searchPage = goToAddPage().insert(randomString());

        assertThat(searchPage.getPagePartFromUrl(), is(Pages.getSearchPageUrl()));
    }

    public void assertValid(Menu menu) {
        assertThat(menu.getIds(), contains(
                "menu_Search", "menu_Add", "menu_ClearData", "menu_InsertData"));
    }
}
