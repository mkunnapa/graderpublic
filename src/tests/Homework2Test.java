package tests;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.*;
import java.util.regex.*;

import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class Homework2Test {

    public String BASE_URL = "http://mkalmo-hw2.appspot.com";

    private Browser browser;
    private Browser secondBrowser;

    @Before
    public void setUp() {
        browser = new Browser(BASE_URL);
        secondBrowser = new Browser(BASE_URL);
    }

    @Test
    public void homePageCreatesSession_3() {
        browser.goToHomePage();

        assertThat(browser.findSessionId(), is(notNullValue()));
    }

    @Test
    public void sessionIdStaysTheSameOnReturn_2() {
        String idA = browser.goToHomePage().findSessionId();
        String idB = browser.goToHomePage().findSessionId();

        assertThat(idA, is(notNullValue()));
        assertThat(idA, is(equalTo(idB)));
    }

    @Test
    public void differentUsersHaveDifferentSessions_2() {
        String idA = browser.goToHomePage().findSessionId();
        String idB = secondBrowser.goToHomePage().findSessionId();

        assertThat(idA, is(not(equalTo(idB))));
    }

    @Test
    public void newSessionIncreasesSessionCount_3() {

        int sessionCountBefore =
                browser.goToSessionCountPage().findSessionCount();

        secondBrowser.goToHomePage();

        int sessionCountAfter =
                browser.goToSessionCountPage().findSessionCount();

        assertThat(sessionCountBefore, is(sessionCountAfter - 1));
    }

    @Test
    public void logOutDecreasesSessionCount_1() {

        browser.goToHomePage();

        int sessionCountBefore =
                browser.goToSessionCountPage().findSessionCount();

        browser.goToLogOutPage();

        int sessionCountAfter =
                browser.goToSessionCountPage().findSessionCount();

        assertThat(sessionCountBefore,
                   is(equalTo(sessionCountAfter + 1)));
    }

    @Test
    public void sessionAttributeCanBeSet_1() {

        String paramValue = getRandomString();

        browser.goToHomePage("?param=" + paramValue);

        String sessionAttribute =
                browser.goToHomePage().findSessionAttribute();

        assertThat(sessionAttribute, is(equalTo(paramValue)));
    }

    @Test
    public void sessionAttributesAreGoneAfterLogout_2() {

        String paramValue = getRandomString();

        browser.goToHomePage("?param=" + paramValue);
        browser.goToLogOutPage();

        String sessionAttribute =
                browser.goToHomePage().findSessionAttribute();

        assertThat(sessionAttribute, is("null"));
    }

    @AfterClass
    public static void closeOpenDrivers() {
        Browser.closeOpenDrivers();
    }

    private static class Browser {

        private static Pattern SESSION_ID_PATTERN =
                Pattern.compile("id: +(\\w+)", Pattern.CASE_INSENSITIVE);
        private static Pattern SESSION_COUNT_PATTERN =
                Pattern.compile("count: (\\d+)", Pattern.CASE_INSENSITIVE);
        private static Pattern SESSION_ATTRIBUTE_PATTERN =
                Pattern.compile("attribute: (\\w+)", Pattern.CASE_INSENSITIVE);

        private String baseUrl;
        private WebDriver driver;

        public Browser(String baseUrl) {
            this.baseUrl = baseUrl;
            driver = getDriver();
        }

        @SuppressWarnings("unused")
        public String getPageSource() {
            return driver.getPageSource();
        }

        public Browser goToHomePage(String params) {
            driver.get(getUrl("HomePage" + params));
            return this;
        }

        public Browser goToHomePage() {
            driver.get(getUrl("HomePage"));
            return this;
        }

        public Browser goToLogOutPage() {
            driver.get(getUrl("LogOut"));
            return this;
        }

        public Browser goToSessionCountPage() {
            driver.get(getUrl("SessionCount"));
            return this;
        }

        public String findSessionId() {
            return matchCommon(driver.getPageSource(), SESSION_ID_PATTERN);
        }

        public String findSessionAttribute() {
            return matchCommon(driver.getPageSource(), SESSION_ATTRIBUTE_PATTERN);
        }

        public int findSessionCount() {
            String countAsString = matchCommon(driver.getPageSource(), SESSION_COUNT_PATTERN);
            return countAsString != null ? Integer.valueOf(countAsString) : 0;
        }

        private String matchCommon(String src, Pattern pattern) {
            Matcher m = pattern.matcher(src);
            if (m.find()) {
                return m.group(1);
            } else {
                throw new IllegalStateException("pattern not found");
            }
        }

        public static void closeOpenDrivers() {
            for (WebDriver each : openDriversDrivers) {
                each.close();
            }
        }

        private static Set<WebDriver> openDriversDrivers = new HashSet<WebDriver>();

        private WebDriver getDriver() {
            WebDriver driver = new HtmlUnitDriver();
            openDriversDrivers.add(driver);
            return driver;
        }

        private String getUrl(String page) {
            String url = System.getProperty("batchBaseUrl");
            if (url == null) {
                url = baseUrl;
            }

            return url + "/" + page;
        }
    }

    private String getRandomString() {
        return String.valueOf(System.currentTimeMillis());
    }


}
