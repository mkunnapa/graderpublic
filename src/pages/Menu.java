package pages;

import java.util.List;

import org.openqa.selenium.*;

public class Menu extends Pages {

    public Menu(WebDriver driver) {
        this.driver = driver;
    }

    public List<String> getIds() {
        return getIdsStartingWith("menu_");
    }

    public SearchPage clearData() {
        element("menu_ClearData").click();
        return Pages.createSearchPage(driver);
    }

    public SearchPage insertSampleData() {
        element("menu_InsertData").click();
        return Pages.createSearchPage(driver);
    }

    public SearchPage selectFirstLanguage() {
        getLanguageLinks().get(0).click();
        return Pages.createSearchPage(driver);
    }

    public SearchPage selectSecondLanguage() {
        getLanguageLinks().get(1).click();
        return Pages.createSearchPage(driver);
    }

    private List<WebElement> getLanguageLinks() {
        List<WebElement> links = element("langButtons")
                .findElements(By.tagName("a"));
        return links;
    }

    public AddPage addPage() {
        element("menu_Add").click();
        return Pages.createAddPage(driver);
    }

    public LogInPage logOut() {
        element("menu_logOut").click();
        return new LogInPage(driver);
    }


}
