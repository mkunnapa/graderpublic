package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;

public class SearchPage extends Pages {

    public String url;

    public SearchPage(String pageUrl, WebDriver driver) {
        this.url = pageUrl;
        this.driver = driver;
    }

    public SearchPage goTo() {
        goTo(url);
        return new SearchPage(url, driver);
    }

    public Menu menu() {
        return new Menu(driver);
    }

    public void delete(String code) {
        element("delete_" + code).click();
    }

    public ViewPage view(String code) {
        element("view_" + code).click();
        return new ViewPage(driver);
    }

    public SearchPage search(String searchString) {
        element("searchStringBox").sendKeys(searchString);
        element("filterButton").click();
        return new SearchPage(url, driver);
    }

    public List<String> getCodes() {
        List<String> rowIds = getIdsStartingWith("row_");
        List<String> codes = new ArrayList<String>();
        for (String rowId : rowIds) {
            codes.add(rowId.replace("row_", ""));
        }

        return codes;
    }

}
