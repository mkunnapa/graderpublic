import java.text.MessageFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;
import org.junit.runner.*;
import org.junit.runner.notification.*;

import tests.*;

public class BatchRunner {
    private static final String BASE_URL = "http://localhost:8080/";

    private static final Pattern MINUS_POINTS_PATTERN = Pattern.compile(
            "\\w+_(\\d+)", Pattern.CASE_INSENSITIVE);

    private static final Class<?> TESTS_TO_RUN = Homework6Test.class;

    @Test
    public void runSingle() {
        String userName = "...";
        runAllTests(userName, userName);
    }

    @Test
    public int runAllTests(String userName, String name) {
        JUnitCore junit = new JUnitCore();
        final PointCounter counter = new PointCounter(10);
        junit.addListener(new RunListener() {
            @Override
            public void testFailure(Failure failure) throws Exception {
                int minusPoints = getMinusPointCount(failure.getDescription()
                        .toString());
                counter.subtract(minusPoints);
                System.out.println("   " + failure.getDescription());
            }
        });

        System.out.println(MessageFormat.format("{0} ({1})", name, userName));

        System.setProperty("batchBaseUrl", BASE_URL);
        junit.run(TESTS_TO_RUN);

        System.out.println("POINTS: " + counter.getResult());
        System.out.println();
        return counter.getResult();
    }

    private int getMinusPointCount(String src) {
        return Integer.parseInt(matchCommon(src, MINUS_POINTS_PATTERN));
    }

    public static String matchCommon(String src, Pattern pattern) {
        Matcher m = pattern.matcher(src);
        if (m.find()) {
            return m.group(1);
        } else {
            throw new IllegalStateException("pattern not found");
        }
    }

    class PointCounter {
        int count;

        public PointCounter(int max) {
            this.count = max;
        }

        public int getResult() {
            return Math.max(0, count);
        }

        public void subtract(int howMany) {
            count -= howMany;
        }
    }
}
